## 0.4

- Refactoring
- Temporary deleted external users function

## 0.5

- [X] Relationship between users in DM simplified
- [ ] More customization in html graph
- [X] Html and json properties
- [X] Add security fix for usernames