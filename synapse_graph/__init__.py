"""This module shows a map of your home Matrix server."""

from .graph import SynapseGraph, SynapseGraphError

__version__ = "0.5.4"
