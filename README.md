## This module shows a map of your home Matrix server.

> "An administrator token is required for use."

There is an example usage in the `main.py` file.


-> [CHANGELOG](CHANGELOG.md)